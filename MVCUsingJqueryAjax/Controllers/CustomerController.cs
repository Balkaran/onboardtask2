﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using MVCUsingJqueryAjax.Models;
using Newtonsoft.Json;
using System.Web.Http;
using System.Web.Http.Description;

namespace MVCUsingJqueryAjax.Controllers
{
	public class CustomerController : Controller
	{
		MVCJqueryDBContext db = new MVCJqueryDBContext();

		public ActionResult Index()
		{

			return View();
		}

		public ActionResult CustomerIndex()
		{

			return View();
		}

		public JsonResult GetCustomerList()
		{
			List<ModelViewClass> custList = db.Customers.Select(x => new ModelViewClass
			{
				CustomerId = x.CustomerId,
				CustomerName = x.CustomerName,
				CustomerAddress = x.CustomerAddress
			}).ToList();
			return Json(custList, JsonRequestBehavior.AllowGet);

		}

		public JsonResult GetCustomerById(int CustomerId)
		{
			Customer cust = db.Customers.Where(x => x.CustomerId == CustomerId).SingleOrDefault();
			string value = string.Empty;
			value = JsonConvert.SerializeObject(cust, Formatting.Indented, new JsonSerializerSettings
			{
				ReferenceLoopHandling = ReferenceLoopHandling.Ignore
			});
			return Json(value, JsonRequestBehavior.AllowGet);
		}

		public JsonResult SaveDataInDatabase(ModelViewClass mvc)
		{
			var result = false;
			try
			{
				if (mvc.CustomerId > 0)
				{
					Customer cust = db.Customers.SingleOrDefault(x => x.CustomerId == mvc.CustomerId);
					cust.CustomerName = mvc.CustomerName;
					cust.CustomerAddress = mvc.CustomerAddress;
					db.SaveChanges();
					result = true;
				}
				else
				{
					Customer cust = new Customer();
					cust.CustomerId = mvc.CustomerId;
					cust.CustomerName = mvc.CustomerName;
					cust.CustomerAddress = mvc.CustomerAddress;
					db.Customers.Add(cust);
					db.SaveChanges();
					result = true;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Json(result, JsonRequestBehavior.AllowGet);
		}

		public JsonResult DeleteCustomerRecord(int CustomerId)
		{
			bool result = false;
			Customer cus = db.Customers.SingleOrDefault(x => x.IsDeleted == false && x.CustomerId == CustomerId);
			if (cus != null)
			{
				cus.IsDeleted = true;
				db.Customers.Remove(cus);
				db.SaveChanges();
				result = true;
			}

			return Json(result, JsonRequestBehavior.AllowGet);
		}
	}
}