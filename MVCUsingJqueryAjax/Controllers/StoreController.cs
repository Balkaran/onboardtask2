﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using MVCUsingJqueryAjax.Models;
using Newtonsoft.Json;

namespace MVCUsingJqueryAjax.Controllers
{
	public class StoreController : Controller
	{
		MVCJqueryDBContext db = new MVCJqueryDBContext();
		// GET: Store
		public ActionResult Index()
		{
			//List<ProductSold> prosold = db.ProductSolds.ToList();
			//ViewBag.ListOfStores = new SelectList(prosold, "StoreId", "StoreName");
			return View();
		}


		public JsonResult GetStoreList()
		{
			List<ModelViewClass> storeList = db.Stores.Select(x => new ModelViewClass
			{
				StoreId = x.StoreId,
				StoreName = x.StoreName,
				StoreAddress = x.StoreAddress
			}).ToList();

			return Json(storeList, JsonRequestBehavior.AllowGet);

		}

		public JsonResult GetStoreById(int StoreId)
		{
			Store store = db.Stores.Where(x => x.StoreId == StoreId).SingleOrDefault();
			string value = string.Empty;
			value = JsonConvert.SerializeObject(store, Formatting.Indented, new JsonSerializerSettings
			{
				ReferenceLoopHandling = ReferenceLoopHandling.Ignore
			});
			return Json(value, JsonRequestBehavior.AllowGet);
		}

		public JsonResult SaveDataInDatabase(ModelViewClass mvc)
		{
			var result = false;
			try
			{
				if (mvc.StoreId > 0)
				{
					Store store = db.Stores.SingleOrDefault(x => x.StoreId == mvc.StoreId);
					store.StoreName = mvc.StoreName;
					store.StoreAddress = mvc.StoreAddress;
					db.SaveChanges();
					result = true;
				}
				else
				{
					Store store = new Store();
					store.StoreId = mvc.StoreId;
					store.StoreName = mvc.StoreName;
					store.StoreAddress = mvc.StoreAddress;
					db.Stores.Add(store);
					db.SaveChanges();
					result = true;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Json(result, JsonRequestBehavior.AllowGet);
		}

		public JsonResult DeleteStoreRecord(int StoreId)
		{
			bool result = false;
			Store store = db.Stores.SingleOrDefault(x => x.StoreId == StoreId);
			if (store != null)
			{
				//cus.IsDeleted = true;
				db.Stores.Remove(store);
				db.SaveChanges();
				result = true;
			}

			return Json(result, JsonRequestBehavior.AllowGet);
		}
	}
}