﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using MVCUsingJqueryAjax.Models;
using Newtonsoft.Json;

namespace MVCUsingJqueryAjax.Controllers
{
    public class ProductSoldController : Controller
    {
		MVCJqueryDBContext db = new MVCJqueryDBContext();

		// GET: ProductSold
		public ActionResult Index()
        {
			List<Customer> proCust = db.Customers.ToList();
			ViewBag.ListOfCustomers = new SelectList(proCust, "CustomerId","CustomerName");

			List<Product> proProd = db.Products.ToList();
			ViewBag.ListOfProducts= new SelectList(proProd, "ProductId", "ProductName");

			List<Store> proStor = db.Stores.ToList();
			ViewBag.ListOfStores = new SelectList(proStor, "StoreId", "StoreName");

			return View();
        }

		public JsonResult GetSalesList()
		{
			List<ModelViewClass> salesList = db.ProductSolds.Select(x => new ModelViewClass
			{
				ProductSoldId = x.ProductSoldId,
				CustomerName = x.Customer.CustomerName,
				ProductName = x.Product.ProductName,
				StoreName = x.Store.StoreName,
				DateSold = x.DateSold
			}).ToList();

			return Json(salesList, JsonRequestBehavior.AllowGet);
		}
		public JsonResult SaveDataInDatabase(ModelViewClass mvc)
		{
			var result = false;
			try
			{
				if (mvc.ProductSoldId > 0)
				{
					ProductSold proSold = db.ProductSolds.SingleOrDefault(x => x.ProductSoldId == mvc.ProductSoldId);
					proSold.CustomerId = mvc.CustomerId;
					proSold.ProductId = mvc.ProductId;
					proSold.StoreId = mvc.StoreId;
					proSold.DateSold = mvc.DateSold;
					db.SaveChanges();
					result = true;
				}
				else
				{
					ProductSold proSold = new ProductSold();
					proSold.ProductSoldId = mvc.ProductSoldId;
					proSold.CustomerId = mvc.CustomerId;
					proSold.ProductId = mvc.ProductId;
					proSold.StoreId = mvc.StoreId;
					proSold.DateSold = mvc.DateSold;
					db.ProductSolds.Add(proSold);
					db.SaveChanges();
					result = true;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Json(result, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetSalesById(int ProductSoldId)
		{
			ProductSold proSold = db.ProductSolds.Where(x => x.ProductSoldId == ProductSoldId).Include(c => c.Customer).Include(p=>p.Product).Include(s=>s.Store).SingleOrDefault();
			string value = string.Empty;
			value = JsonConvert.SerializeObject(proSold, Formatting.Indented, new JsonSerializerSettings
			{
				ReferenceLoopHandling = ReferenceLoopHandling.Ignore
			});
			return Json(value, JsonRequestBehavior.AllowGet);
		}

		public JsonResult DeleteSalesRecord(int ProductSoldId)
		{
			bool result = false;
			ProductSold pro = db.ProductSolds.SingleOrDefault(x => x.ProductSoldId == ProductSoldId);
			if (pro != null)
			{
				//cus.IsDeleted = true;
				db.ProductSolds.Remove(pro);
				db.SaveChanges();
				result = true;
			}

			return Json(result, JsonRequestBehavior.AllowGet);
		}
	}
}