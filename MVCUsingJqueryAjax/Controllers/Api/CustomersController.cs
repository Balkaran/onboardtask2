﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MVCUsingJqueryAjax.Models;

namespace MVCUsingJqueryAjax.Controllers.Api
{
    public class CustomersController : ApiController
    {
        private MVCJqueryDBContext db = new MVCJqueryDBContext();

		//[Route("customer/api/customers")]
		[HttpGet]
		// GET: api/Customers	
		public IQueryable<Customer> Get()
		{
			return db.Customers;
		}
		//[HttpGet]
		//// GET: api/Customers/5
		//[ResponseType(typeof(Customer))]
		//      public async Task<IHttpActionResult> GetCustomer(int id)
		//      {
		//          Customer customer = await db.Customers.FindAsync(id);
		//          if (customer == null)
		//          {
		//              return NotFound();
		//          }

		//          return Ok(customer);
		//      }

		// PUT: api/Customers/5
		[HttpPut]
		[ResponseType(typeof(void))]
		public IHttpActionResult PutCustomer([FromUri] int id, [FromBody] Customer customer)
		{
			var result = false;
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (id != customer.CustomerId)
			{
				return BadRequest();
			}

			db.Entry(customer).State = EntityState.Modified;

			try
			{
				db.SaveChanges();
				result = true;
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!CustomerExists(id))
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return Ok(result);
			//return StatusCode(HttpStatusCode.NoContent);
		}

		//// PUT: api/Customers/5
		//[HttpPut]
		//[ResponseType(typeof(void))]
		//public async Task<IHttpActionResult> PutCustomer(int id, Customer customer)
		//{
		//	if (!ModelState.IsValid)
		//	{
		//		return BadRequest(ModelState);
		//	}

		//	if (id != customer.CustomerId)
		//	{
		//		return BadRequest();
		//	}

		//	db.Entry(customer).State = EntityState.Modified;

		//	try
		//	{
		//		await db.SaveChangesAsync();
		//	}
		//	catch (DbUpdateConcurrencyException)
		//	{
		//		if (!CustomerExists(id))
		//		{
		//			return NotFound();
		//		}
		//		else
		//		{
		//			throw;
		//		}
		//	}

		//	return StatusCode(HttpStatusCode.NoContent);
		//}

		// //     // POST: api/Customers
		// //     [ResponseType(typeof(Customer))]
		// //     public async Task<IHttpActionResult> PostCustomer(Customer customer)
		// //     {
		// //         if (!ModelState.IsValid)
		// //         {
		// //             return BadRequest(ModelState);
		// //         }

		// //         db.Customers.Add(customer);
		// //         await db.SaveChangesAsync();

		//	////return Ok(customer);

		// //         return CreatedAtRoute("DefaultApi", new { id = customer.CustomerId }, customer);
		// //     }

		// POST: api/Customers
		[ResponseType(typeof(Customer))]
		[HttpPost]
		public IHttpActionResult PostCustomer([FromBody]Customer customer)
		{
			var result = false;
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			db.Customers.Add(customer);
			db.SaveChanges();
			result = true;

			//return Ok(customer);

			return CreatedAtRoute("DefaultApi", new { id = customer.CustomerId }, result);
		}

		// DELETE: api/Customers/5
		[HttpDelete]
        public IHttpActionResult DeleteCustomer(int id)
        {
			var result = false;
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return NotFound();
            }

			customer.IsDeleted = true;
            db.Customers.Remove(customer);
            db.SaveChanges();
			result = true;

            return Ok(result);
        }

		//[ResponseType(typeof(Customer))]
		//public async Task<IHttpActionResult> DeleteCustomer(int CustomerId)
		//{
		//	var result = false;
		//	Customer customer = await db.Customers.FindAsync(CustomerId);
		//	if (customer == null)
		//	{
		//		return NotFound();
		//	}

		//	customer.IsDeleted = true;
		//	db.Customers.Remove(customer);
		//	await db.SaveChangesAsync();
		//	result = true;

		//	return Ok(result);
		//}

		protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CustomerExists(int id)
        {
            return db.Customers.Count(e => e.CustomerId == id) > 0;
        }
    }
}