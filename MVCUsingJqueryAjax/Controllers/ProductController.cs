﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using MVCUsingJqueryAjax.Models;
using Newtonsoft.Json;

namespace MVCUsingJqueryAjax.Controllers
{
    public class ProductController : Controller
    {
		MVCJqueryDBContext db = new MVCJqueryDBContext();

		// GET: Product
		public ActionResult Index()
        {
			//List<ProductSold> prosold = db.ProductSolds.ToList();
			//ViewBag.ListOfProducts = new SelectList(prosold, "ProductId", "ProductName");
			return View();
        }

		public JsonResult GetProductList()
		{
			List<ModelViewClass> proList = db.Products.Select(x => new ModelViewClass
			{
				ProductId = x.ProductId,
				ProductName = x.ProductName,
				ProductPrice = x.ProductPrice
			}).ToList();
			return Json(proList, JsonRequestBehavior.AllowGet);

		}

		public JsonResult GetProductById(int ProductId)
		{
			Product prod = db.Products.Where(x => x.ProductId == ProductId).SingleOrDefault();
			string value = string.Empty;
			value = JsonConvert.SerializeObject(prod, Formatting.Indented, new JsonSerializerSettings
			{
				ReferenceLoopHandling = ReferenceLoopHandling.Ignore
			});
			return Json(value, JsonRequestBehavior.AllowGet);
		}

		public JsonResult SaveDataInDatabase(ModelViewClass mvc)
		{
			var result = false;
			try
			{
				if (mvc.ProductId > 0)
				{
					Product prod = db.Products.SingleOrDefault(x => x.ProductId == mvc.ProductId);
					prod.ProductName = mvc.ProductName;
					prod.ProductPrice = mvc.ProductPrice;
					db.SaveChanges();
					result = true;
				}
				else
				{
					Product prod = new Product();
					prod.ProductId = mvc.ProductId;
					prod.ProductName = mvc.ProductName;
					prod.ProductPrice = mvc.ProductPrice;
					db.Products.Add(prod);
					db.SaveChanges();
					result = true;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Json(result, JsonRequestBehavior.AllowGet);
		}

		public JsonResult DeleteProductRecord(int ProductId)
		{
			bool result = false;
			Product pro = db.Products.SingleOrDefault(x =>x.ProductId == ProductId);
			if (pro != null)
			{
				//cus.IsDeleted = true;
				db.Products.Remove(pro);
				db.SaveChanges();
				result = true;
			}

			return Json(result, JsonRequestBehavior.AllowGet);
		}
	}
}