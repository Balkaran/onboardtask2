﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MVCUsingJqueryAjax.Models
{
	public class ProductSold
	{
		[Key]
		public int ProductSoldId { get; set; }
		public int ProductId { get; set; }
		public int CustomerId { get; set; }
		public int StoreId { get; set; }

		[DataType(DataType.DateTime)]
		public DateTime DateSold { get; set; }

		public  Customer Customer { get; set; }
		public  Product Product { get; set; }
		public  Store Store { get; set; }
	}
}