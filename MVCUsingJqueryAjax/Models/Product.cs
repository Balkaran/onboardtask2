﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MVCUsingJqueryAjax.Models
{
	public class Product
	{
		[Key]
		public int ProductId { get; set; }

		[Required(ErrorMessage = "Product Name is required")]
		[StringLength(100, MinimumLength = 3)]
		public string ProductName { get; set; }

		//[Range(1,100)]
		[DataType(DataType.Currency)]
		public decimal ProductPrice { get; set; }

		//public bool IsDeleted { get; set; }

		public ICollection<ProductSold> ProductsSolds { get; set; }
	}
}