﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MVCUsingJqueryAjax.Models
{
	public class ModelViewClass
	{
		[Key]
		public int CustomerId { get; set; }

		[Required(ErrorMessage = "Customer Name is required")]
		[StringLength(100, MinimumLength = 3)]
		public string CustomerName { get; set; }

		[Required(ErrorMessage = "Customer Address is required")]
		[StringLength(200, MinimumLength = 5)]
		public string CustomerAddress { get; set; }

		public bool IsDeleted { get; set; }

		[Key]
		public int ProductId { get; set; }

		[Required(ErrorMessage = "Product Name is required")]
		[StringLength(100, MinimumLength = 3)]
		public string ProductName { get; set; }

		//[Range(1,100)]
		[DataType(DataType.Currency)]
		public decimal ProductPrice { get; set; }

		[Key]
		public int StoreId { get; set; }

		[Required(ErrorMessage = "Store Name is required")]
		[StringLength(100, MinimumLength = 3)]
		public string StoreName { get; set; }

		[Required(ErrorMessage = "Store Address is required")]
		[StringLength(100, MinimumLength = 3)]
		public string StoreAddress { get; set; }

		[Key]
		public int ProductSoldId { get; set; }
		[DataType(DataType.DateTime)]
		public DateTime DateSold { get; set; }


	}
}