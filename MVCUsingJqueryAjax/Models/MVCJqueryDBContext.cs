﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MVCUsingJqueryAjax.Models
{
	public class MVCJqueryDBContext:DbContext
	{
		public MVCJqueryDBContext() : base("name=MVC_Jquery_DB") { }

		public DbSet<Customer> Customers { get; set; }
		public DbSet<Product> Products { get; set; }
		public DbSet<Store> Stores { get; set; }
		public DbSet<ProductSold> ProductSolds { get; set; }
	}
}