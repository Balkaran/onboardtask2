﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MVCUsingJqueryAjax.Models
{
	public class Store
	{
		[Key]
		public int StoreId { get; set; }

		[Required(ErrorMessage = "Store Name is required")]
		[StringLength(100, MinimumLength = 3)]
		public string StoreName { get; set; }

		[Required(ErrorMessage = "Store Address is required")]
		[StringLength(100, MinimumLength = 3)]
		public string StoreAddress { get; set; }

		//public bool IsDeleted { get; set; }

		public ICollection<ProductSold> ProductsSolds { get; set; }
	}
}